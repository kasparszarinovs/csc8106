<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:url var="resourcesRoot" value="/resources/" />
<c:url var="siteRoot" value="/" />

<!doctype html>
<html>
<head>
    <title>Sign up</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="stylesheet" href="${resourcesRoot}css/normalize.css" type="text/css" />
    <link rel="stylesheet" href="${resourcesRoot}css/style.css" type="text/css" />
</head>
<body>
<div class="wrapper">
    <div class="container">
        <div class="title">CSC8106 Web Application</div>

        <div class="content login">
            <c:if test="${error}"><div class="error">Incorrect login name / password.</div></c:if>
            <c:if test="${success}"><div class="success">Account successfully created!</div></c:if>
            <form method="post">
                <fieldset>
                    <label for="username">Username</label>
                    <input type="email" name="username" id="username" />
                </fieldset>
                <fieldset>
                    <label for="password">Password</label>
                    <input type="password" name="password" id="password" />
                </fieldset>
                <fieldset>
                    <input type="submit" value="Sign up" />
                </fieldset>
            </form>
            <div class="info">Already have an account?<br/><a href="${siteRoot}login">Login</a></div>
        </div>
    </div>
</div>

<script type="text/javascript" src="${resourcesRoot}js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="${resourcesRoot}js/script.js"></script>
</body>
</html>
