<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:url var="resourcesRoot" value="/resources/" />
<c:url var="siteRoot" value="/" />

<!doctype html>
<html>
<head>
	<title>Home</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	
	<link rel="stylesheet" href="${resourcesRoot}css/normalize.css" type="text/css" />
	<link rel="stylesheet" href="${resourcesRoot}css/style.css" type="text/css" />
</head>
<body>
<div class="wrapper">
	<div class="container">
		<div class="title">CSC8106 Web Application</div>
	
		<header>
			<nav class="cf">
				<ul class="secondary-menu">
                    <li><a href="${siteRoot}logout">Logout</a></li>
				</ul>
			</nav>
		</header>
		
		<div class="content home">
            <h1>Welcome ${profile.getFirstName()} ${profile.getLastName()}</h1>
            <div>You have successfully logged in!</div>
		</div>
		
	</div>
</div>

<script type="text/javascript" src="${resourcesRoot}js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="${resourcesRoot}js/script.js"></script>
</body>
</html>