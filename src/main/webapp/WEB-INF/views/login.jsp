<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:url var="resourcesRoot" value="/resources/" />
<c:url var="siteRoot" value="/" />

<!doctype html>
<html>
<head>
	<title>Login</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	
	<link rel="stylesheet" href="${resourcesRoot}css/normalize.css" type="text/css" />
	<link rel="stylesheet" href="${resourcesRoot}css/style.css" type="text/css" />
</head>
<body>
<div class="wrapper">
    <div class="container">
        <div class="title">CSC8106 Web Application</div>

        <div class="content login">
            <c:if test="${error}"><div class="error">Incorrect login name / password.</div></c:if>
            <form method="post">
                <fieldset>
                    <label for="username">Username</label>
                    <input type="email" name="username" id="username" required />
                </fieldset>
                <fieldset>
                    <label for="password">Password</label>
                    <input type="password" name="password" id="password" required />
                </fieldset>
                <fieldset>
                    <input type="submit" value="Log in" />
                </fieldset>
            </form>
            <div class="info">Don't have an account? <a href="${siteRoot}signup" title="Sign up">Sign up</a><br/><br/>Or log in with one of your social network accounts</div>
            <div id="overlayidentifier"></div>
        </div>

    </div>
</div>


<script type="text/javascript">
    document.write(unescape("%3Cscript src='https://login.idfederate.com/static/js/connect.js' type='text/javascript'%3E%3C/script%3E"));
</script>

<script type="text/javascript">
    CONNECT.type = 'embedded';
    CONNECT.providers =['Facebook','Google','LinkedIn'];
    CONNECT.language = 'en';
    CONNECT.login_url = 'https://itec-csc8106.idfederate.com';
    CONNECT.token_url = 'http://localhost:8080<%= request.getContextPath() %>/auth_token/';
    CONNECT.flow_type= 'redirect';
    CONNECT.configureConnect();
</script>

<script type="text/javascript" src="${resourcesRoot}js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="${resourcesRoot}js/script.js"></script>
</body>
</html>
