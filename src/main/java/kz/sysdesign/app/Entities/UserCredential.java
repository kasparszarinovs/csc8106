package kz.sysdesign.app.Entities;

import java.io.Serializable;

import javax.persistence.*;

@Entity
public class UserCredential implements Serializable {

	private Long id;
	private String name;
	private String password;
	private String salt;
	private Integer federated;
    private UserInfo userInfo;
	
	public UserCredential() {  }
	
	public UserCredential(String name, String password, String salt, Integer federated, UserInfo userInfo) {
		this.name = name;
		this.password = password;
		this.salt = salt;
		this.federated = federated;
        this.userInfo = userInfo;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	@Column(nullable=false, unique=true)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	@Column(nullable=false)
	public Integer getFederated() {
		return federated;
	}

	public void setFederated(Integer federated) {
		this.federated = federated;
	}

    @ManyToOne
    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }
	
	@Override
	public String toString() {
		return name;
	}

}
