package kz.sysdesign.app.Entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

@Entity
public class UserInfo implements Serializable {

	private Long id;
	private String firstName;
	private String lastName;
	private String email;
    private String address;
    private String shortBio;
	private List<UserCredential> boundUserCredentials;
	
	public UserInfo () {  }
	
	public UserInfo(String firstName, String lastName, String email, String address) {
		this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.address = address;
    }

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getShortBio() {
        return shortBio;
    }

    public void setShortBio(String shortBio) {
        this.shortBio = shortBio;
    }

    @OneToMany
    public List<UserCredential> getBoundUserCredentials() {
        return boundUserCredentials;
    }

    public void setBoundUserCredentials(List<UserCredential> boundUserCredentials) {
        this.boundUserCredentials = boundUserCredentials;
    }
	
}
