package kz.sysdesign.app.Controllers;

import java.io.IOException;

import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import kz.sysdesign.app.Entities.UserCredential;
import kz.sysdesign.app.Entities.UserInfo;
import kz.sysdesign.app.Helpers.AuthHelper;
import kz.sysdesign.app.Services.UserCredentialService;
import kz.sysdesign.app.Services.UserInfoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import uk.co.cloudidentity.connect.client.config.jaxb.Config;
import uk.co.cloudidentity.connect.client.request.AuthorizationRequest;
import uk.co.cloudidentity.connect.client.response.AuthenticationResponse;
import uk.co.cloudidentity.connect.client.social.BasicConnectService;
import uk.co.cloudidentity.connect.common.API;
import uk.co.cloudidentity.connect.common.domain.api.Profile;

@Controller
public class AppController {

	@Autowired private UserCredentialService ucService;
	@Autowired private UserInfoService uiService;
	@Autowired private Config connectConfig;
	
	private final String SESSION_AUTH_KEY = "auth_key";
    private final String SESSION_AUTH_PROFILE = "auth_profile";

    @RequestMapping(value="/", method=RequestMethod.GET)
    public String home(@RequestParam(required=false) boolean error, Model model, HttpServletRequest request) {
        if(AuthHelper.getAuth(request, SESSION_AUTH_KEY) == null) {
            if(error)
                model.addAttribute("error", error);
            return "login";
        }

        model.addAttribute("logedin", true);
        model.addAttribute("profile", request.getSession().getAttribute(SESSION_AUTH_PROFILE));

        return "home";
    }

	@RequestMapping(value="/login", method=RequestMethod.GET)
	public String login(@RequestParam(required=false) boolean error, Model model, HttpServletRequest request) {
        if(AuthHelper.getAuth(request, SESSION_AUTH_KEY) != null)
			return "redirect:/";
		
		if(error)
			model.addAttribute("error", error);
		
		return "login";
	}
	
	@RequestMapping(value={"/login", "/"}, method=RequestMethod.POST)
	public String authenticate(@RequestParam String username, @RequestParam String password, Model model, HttpServletRequest request) {
		if(username.length() < 1 || password.length() < 1)
			return "redirect:" + request.getServletPath() + "?error=true";
		
		try {
            UserCredential uc = ucService.authenticate(username, password);
			if(uc == null)
                return "redirect:" + request.getServletPath() + "?error=true";

            HttpSession session = request.getSession();
            UserInfo ui = uc.getUserInfo();

            session.setAttribute(SESSION_AUTH_KEY, uc);
            session.setAttribute(SESSION_AUTH_PROFILE, ui);

            return "redirect:/";
		}
        catch(NonUniqueResultException e) {  }
        catch(NoResultException e) {  }

        return "redirect:" + request.getServletPath() + "?error=true";
	}
	
	@RequestMapping(value="/logout", method=RequestMethod.GET)
	public String logout(Model model, HttpServletRequest request) {
		if(AuthHelper.getAuth(request, SESSION_AUTH_KEY) != null)
			request.getSession().invalidate();

		return "redirect:/";
	}

	@RequestMapping(value="/signup", method=RequestMethod.GET)
	public String signup(Model model, HttpServletRequest request) {
		if(AuthHelper.getAuth(request, SESSION_AUTH_KEY) != null)
			return "redirect:/";
		
		return "signup";
	}
	
	@RequestMapping(value="/signup", method=RequestMethod.POST)
	public String signup(@RequestParam String username, @RequestParam String password, Model model, HttpServletRequest request) {
		if(username.length() < 1 || password.length() < 1)
			model.addAttribute("error", true);
		
		try {
            UserCredential uc = ucService.add(username, password, 0);
            request.getSession().setAttribute(SESSION_AUTH_KEY, uc);
		}
		catch(RuntimeException e) { model.addAttribute("error", true); }

        model.addAttribute("success", true);

		return "signup";
	}
	
	@RequestMapping(value="/auth_token", method=RequestMethod.POST)
	public String federateLogin(Model model, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        try {
            AuthorizationRequest authRequest = AuthorizationRequest.parse(request);
            String token = authRequest.getToken();
            BasicConnectService connectService = new BasicConnectService(connectConfig);
            AuthenticationResponse authentication = connectService.getAuthentication(token);

            if (API.Status.OK.equals(authentication.getStatus())) {
                final Profile profile = authentication.getProfile();

                UserCredential uc = ucService.authenticate(profile.getIdentifier());
                if(uc == null) {
                    String firstName = profile.getName() != null ? profile.getName().getGivenName() : null;
                    String lastName = profile.getName() != null ? profile.getName().getFamilyName() : null;
                    String address = profile.getAddress() != null ? profile.getAddress().getFormattedAddress() : null;

                    uc = ucService.add(profile.getIdentifier(), 1, firstName, lastName, profile.getEmail(), address);
                }

                UserInfo ui = uc.getUserInfo();
                HttpSession session = request.getSession();

                session.setAttribute(SESSION_AUTH_KEY, uc);
                session.setAttribute(SESSION_AUTH_PROFILE, ui);
            }
            else {
                return "redirect:/login?error=true";
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            return "redirect:/login?error=true";
        }
        
        return "redirect:/";
    }
	
}
