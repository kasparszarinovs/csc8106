package kz.sysdesign.app.DAO;

import kz.sysdesign.app.Entities.UserInfo;

public interface UserInfoDAO {

	public UserInfo add(UserInfo user);

    public boolean delete(UserInfo userInfo);

    public boolean update(UserInfo userInfo);

}
