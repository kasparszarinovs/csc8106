package kz.sysdesign.app.DAO;

import kz.sysdesign.app.Entities.UserCredential;

public interface UserCredentialDAO {

	public UserCredential add(UserCredential userCredentials);

	public boolean delete(UserCredential userCredentials);

	public UserCredential authenticate(String username);

    public UserCredential authenticate(String username, String password);

    public String getSalt(String username);

}
