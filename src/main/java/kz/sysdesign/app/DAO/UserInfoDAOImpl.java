package kz.sysdesign.app.DAO;


import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import kz.sysdesign.app.Entities.UserInfo;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class UserInfoDAOImpl implements UserInfoDAO {
	
	@PersistenceContext(unitName="JpaPersistenceUnit")
	private EntityManager em;
	
	@Transactional
	public UserInfo add(UserInfo userInfo) {
		if(userInfo == null) return null;
		em.persist(userInfo);
		return userInfo;
	}

    public boolean delete(UserInfo userInfo) {
        if(userInfo == null) return false;
        em.remove(userInfo);
        return true;
    }

    @Override
    public boolean update(UserInfo userInfo) {
        if(userInfo == null) return false;
        em.merge(userInfo);
        return true;
    }

}

