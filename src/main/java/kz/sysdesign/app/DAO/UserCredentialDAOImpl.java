package kz.sysdesign.app.DAO;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import kz.sysdesign.app.Entities.UserCredential;

import java.util.List;

@Repository
public class UserCredentialDAOImpl implements UserCredentialDAO {

	@PersistenceContext(unitName="JpaPersistenceUnit")
	private EntityManager em;
	
	public UserCredential add(UserCredential userCredentials) {
		if(userCredentials == null) return null;
		em.persist(userCredentials);
		return userCredentials;
	}

	public boolean delete(UserCredential userCredentials) {
		if(userCredentials == null) return false;
		em.remove(userCredentials);
		return false;
	}
	
	public UserCredential authenticate(String username) {
        if(username == null) return null;
		String jpqlQuery = "FROM " + UserCredential.class.getSimpleName() + " u WHERE lower(u.name) = lower(:username)";
		Query query = em.createQuery(jpqlQuery).setParameter("username", username);

        // List is used to avoid NoResultException
        UserCredential returnCredentials = null;
        List<UserCredential> resultList = (List<UserCredential>) query.getResultList();
        if(!resultList.isEmpty())
            returnCredentials = resultList.get(0);

		return (UserCredential) returnCredentials;
	}

    public UserCredential authenticate(String username, String password) {
        if(username == null) return null;
        String jpqlQuery = "FROM " + UserCredential.class.getSimpleName() + " u WHERE lower(u.name) = lower(:username) AND lower(u.password) = lower(:password)";
        Query query = em.createQuery(jpqlQuery).setParameter("username", username).setParameter("password", password);

        // List is used to avoid NoResultException
        UserCredential returnCredentials = null;
        List<UserCredential> resultList = (List<UserCredential>) query.getResultList();
        if(!resultList.isEmpty())
            returnCredentials = resultList.get(0);

        return (UserCredential) returnCredentials;
    }
	
	public String getSalt(String username) {
		String jpqlQuery = "SELECT u.salt FROM " + UserCredential.class.getSimpleName() + " u WHERE LOWER(u.name) = LOWER(:username)";
		Query query = em.createQuery(jpqlQuery).setParameter("username", username);
		return (String) query.getSingleResult();
	}
	
}
