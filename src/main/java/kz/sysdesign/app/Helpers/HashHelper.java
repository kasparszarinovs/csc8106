package kz.sysdesign.app.Helpers;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class HashHelper {

    public static String generateSalt() {
		SecureRandom sr = new SecureRandom();
		byte[] bytes = new byte[16];
		sr.nextBytes(bytes);
		
		return org.apache.commons.codec.binary.Base64.encodeBase64URLSafeString(bytes);
    }
    
	public static String generateSHA256Hash(String hashable) {
		String hash = "";
		
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.update(hashable.getBytes("UTF-8"));
			byte[] hashedBytes = md.digest();
			
			hash = org.apache.commons.codec.binary.Base64.encodeBase64URLSafeString(hashedBytes);
		} 
		catch(NoSuchAlgorithmException e) {  }
		catch(UnsupportedEncodingException e) {  }
		
		return hash;
	}
	
//	public static String generateMD5Hash(String hashable) {
//		String hash = null;
//		
//		try {
//			MessageDigest md = MessageDigest.getInstance("MD5");
//			md.update(hashable.getBytes("UTF-8"));
//			byte[] hashedBytes = md.digest();
//			
//			hash = org.apache.commons.codec.binary.Base64.encodeBase64URLSafeString(hashedBytes);
//		} 
//		catch(NoSuchAlgorithmException e) {  }
//		catch(UnsupportedEncodingException e) {  }
//		
//		return hash;
//	}
	
	public static String generateMD5Hash(String hashable) {
		String hash = null;
		
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(hashable.getBytes("UTF-8"));
			byte[] hashedBytes = md.digest();
			hash = org.apache.commons.codec.digest.DigestUtils.md5Hex(hashedBytes);
//			hash = org.apache.commons.codec.binary.Base64.encodeBase64URLSafeString(hashedBytes);
		} 
		catch(NoSuchAlgorithmException e) {  }
		catch(UnsupportedEncodingException e) {  }
		
		return hash;
	}
	
}
