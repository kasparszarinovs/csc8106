package kz.sysdesign.app.Helpers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class AuthHelper {
	
	public static Object getAuth(HttpServletRequest request, String authKey) {
		HttpSession session = request.getSession(false);
		
		if(session == null)
			return null;
		else
			return session.getAttribute(authKey);
	}

}
