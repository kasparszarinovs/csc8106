package kz.sysdesign.app.Services;

import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;

import kz.sysdesign.app.DAO.UserCredentialDAO;
import kz.sysdesign.app.DAO.UserInfoDAO;
import kz.sysdesign.app.Entities.UserCredential;
import kz.sysdesign.app.Entities.UserInfo;
import kz.sysdesign.app.Helpers.HashHelper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserCredentialServiceImpl implements UserCredentialService {

	@Autowired private UserCredentialDAO ucDAO;
    @Autowired private UserInfoDAO uiDAO;
	
	@Transactional
	public UserCredential add(String username, Integer federated, String firstName, String lastName, String email, String address) {
		if(username == null || federated == null)
			return null;

        UserInfo ui = new UserInfo(firstName, lastName, email, address);
        uiDAO.add(ui);

        UserCredential uc = new UserCredential(username, null, null, federated, ui);

		return ucDAO.add(uc);
	}

    @Transactional
    public UserCredential add(String username, String password, Integer federated) {
        if(username == null || federated == null)
            return null;

        String salt = null;
        String hashedPassword = null;

        salt = HashHelper.generateSalt();
        hashedPassword = HashHelper.generateSHA256Hash(password + salt);

        UserInfo ui = new UserInfo();
        uiDAO.add(ui);

        UserCredential uc = new UserCredential(username, hashedPassword, salt, federated, ui);

        return ucDAO.add(uc);
    }
	
	public UserCredential authenticate(String username) {
		if(username == null)  return null;
    	return ucDAO.authenticate(username);
	}

    public UserCredential authenticate(String username, String password) {
        if(username == null)  return null;
        String salt = ucDAO.getSalt(username);
        return ucDAO.authenticate(username, HashHelper.generateSHA256Hash(password + salt));
    }
	
}
