package kz.sysdesign.app.Services;

import kz.sysdesign.app.Entities.UserCredential;
import kz.sysdesign.app.Entities.UserInfo;

public interface UserCredentialService {
	
	public UserCredential add(String username, String password, Integer federated);

    public UserCredential add(String username, Integer federated, String firstName, String lastName, String email, String address);
	
	public UserCredential authenticate(String username);

    public UserCredential authenticate(String username, String password);

}
